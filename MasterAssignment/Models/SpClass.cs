﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using static MasterAssignment.Controllers.HomeController;

namespace MasterAssignment.Models
{
    public class SpClass
    {
        private readonly SqlConnection con;
        public SpClass(IConfiguration configuration)
        {
            con = new SqlConnection(configuration.GetConnectionString("DefaultConnection"));
          
        }

        public string InsertOrder(Masterorder masterorder)
        {
            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                decimal amount = 0;
                foreach (var items in masterorder.items)
                {
                    amount += items.unitprice;
                }
            string array = JsonConvert.SerializeObject(masterorder.items);
                SqlCommand cmd = new SqlCommand("InserOrder", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@customerid", masterorder.customerId.ToString());
                cmd.Parameters.AddWithValue("@customerName", masterorder.customerName);
                cmd.Parameters.AddWithValue("@customMobileno", masterorder.customMobileno);
                cmd.Parameters.AddWithValue("@orderno", masterorder.orderno);
                cmd.Parameters.AddWithValue("@paymentmethod", masterorder.paymentmethod);
                cmd.Parameters.AddWithValue("@OrderDate", masterorder.OrderDate);
                cmd.Parameters.AddWithValue("@string", array);
                cmd.Parameters.AddWithValue("@amount", amount);
                string str = cmd.ExecuteScalar().ToString();
                cmd.Dispose();
                con.Close();
                return str;
            }
            catch (Exception ex)
            {
                con.Close();
                return "Error in Order Creating!";
            }
        }
    }
}

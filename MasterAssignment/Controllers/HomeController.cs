﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MasterAssignment.Models;
using Microsoft.Extensions.Configuration;

namespace MasterAssignment.Controllers
{
    public class HomeController : Controller
    {
        private  readonly SpClass spClass;
        public HomeController(IConfiguration configuration)
        {
            spClass = new SpClass(configuration);
        }
        public IActionResult Index()
        {
            return View();
           
        }
        

        [HttpPost]
        public string PlaceOrder([FromBody] Masterorder masterorder)
        {
            if(masterorder == null)
            {
                return "Please Send Correct Json Array";
            }
            if (masterorder.items.Count <= 0)
            {
                return "Please Add items in Order";
            }
            string str = spClass.InsertOrder(masterorder);
            if(str == "ok")
            {
                return "ok";
            }
            return "Error in order creating!";
        }

        public class Masterorder
        {
            public int customerId { get; set; }
            public string customerName { get; set; }
            public string customMobileno { get; set; }
            public string orderno { get; set; }
            public string paymentmethod { get; set; }
            public DateTime OrderDate { get; set; }
            public List<MasterOrderDetail> items { get; set; }
        }
        public class MasterOrderDetail
        {   
            public string productcode { get; set; }
            public string orderSku { get; set; }
            public decimal unitprice { get; set; }
            public string description { get; set; }
        }
    }
}
